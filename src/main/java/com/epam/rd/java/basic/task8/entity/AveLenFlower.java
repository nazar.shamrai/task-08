package com.epam.rd.java.basic.task8.entity;

public class AveLenFlower {
    private int content;
    private String measure;

    public AveLenFlower() {
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "content=" + content +
                ", measure='" + measure + '\'' +
                '}';
    }
}
