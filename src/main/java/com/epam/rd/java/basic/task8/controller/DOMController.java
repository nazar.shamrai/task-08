package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.TagConstants.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private Document document;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		try {
			init();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	private void init() throws ParserConfigurationException, SAXException, IOException {
		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();
		document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();
	}

	public List<Flower> getDocumentFlowerItems() {
		NodeList flowersNodes = document.getElementsByTagName(FLOWER_TAG);
		List<Flower> result = new ArrayList<>();

		for (int i = 0; i < flowersNodes.getLength(); i++) {
			Node flowerNode = flowersNodes.item(i);
			Flower flower = new Flower();

			if (flowerNode.getNodeType() == Node.ELEMENT_NODE) {
				NodeList flowerDetails = flowerNode.getChildNodes();
				for (int j = 0; j < flowerDetails.getLength(); j++) {
					Node detail = flowerDetails.item(j);
					if (detail.getNodeType() == Node.ELEMENT_NODE) {

						Element detailElement = (Element) detail;
						switch (detailElement.getTagName()) {
							case NAME_TAG:
								flower.setName(detailElement.getTextContent());
								break;
							case SOIL_TAG:
								flower.setSoil(detailElement.getTextContent());
								break;
							case ORIGIN_TAG:
								flower.setOrigin(detailElement.getTextContent());
								break;
							case VISUAL_PARAMETERS_TAG:
								VisualParameters vp = new VisualParameters();
								NodeList visualParametersDetails = detail.getChildNodes();
								for (int k = 0; k < visualParametersDetails.getLength(); k++) {
									Node vpDetail = visualParametersDetails.item(k);
									if (vpDetail.getNodeType() == Node.ELEMENT_NODE) {
										Element vpElement = (Element) vpDetail;
										switch (vpElement.getTagName()) {
											case STEM_COLOUR_TAG:
												vp.setStemColor(vpElement.getTextContent());
												break;
											case LEAF_COLOUR_TAG:
												vp.setLeafColour(vpElement.getTextContent());
												break;
											case AVE_LEN_FLOWER_TAG:
												AveLenFlower aveLenFlower = new AveLenFlower();
												aveLenFlower.setContent(Integer.parseInt(vpElement.getTextContent()));
												aveLenFlower.setMeasure(vpElement.getAttribute(MEASURE_TAG));
												vp.setAveLenFlower(aveLenFlower);
												break;
										}
									}
								}
								flower.setVisualParameters(vp);
								break;
							case GROWING_TIPS_TAG:
								GrowingTips gt = new GrowingTips();
								NodeList growingTipsDetails = detail.getChildNodes();
								for (int f = 0; f < growingTipsDetails.getLength(); f++) {
									Node gtDetail = growingTipsDetails.item(f);
									if (gtDetail.getNodeType() == Node.ELEMENT_NODE) {
										Element gtElement = (Element) gtDetail;
										switch (gtElement.getTagName()) {
											case TEMPRETURE_TAG:
												GrowingTips.Temperature temperature = new GrowingTips.Temperature();
												temperature.setContent(Integer.parseInt(gtElement.getTextContent()));
												temperature.setMeasure(gtElement.getAttribute(MEASURE_TAG));
												gt.setTemperature(temperature);
												break;
											case LIGHTING_TAG:
												gt.setLightRequiring(LightRequiring.valueOf(gtElement.getAttribute(LIGHT_REQUIRING_TAG)));
												break;
											case WATERING_TAG:
												Watering watering = new Watering();
												watering.setContent(Integer.parseInt(gtElement.getTextContent()));
												watering.setMeasure(gtElement.getAttribute(MEASURE_TAG));
												gt.setWatering(watering);
												break;
										}
									}
								}

								flower.setGrowingTips(gt);
								break;
							case MULTIPLYING_TAG:
								flower.setMultiplying(detailElement.getTextContent());
								break;
						}

					}
				}
			}

			result.add(flower);
		}
		return result;
	}
}
