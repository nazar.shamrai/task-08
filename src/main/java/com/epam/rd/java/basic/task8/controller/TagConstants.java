package com.epam.rd.java.basic.task8.controller;

public interface TagConstants {
    String TEMPRETURE_TAG = "tempreture";
    String LIGHTING_TAG = "lighting";
    String LIGHT_REQUIRING_TAG = "lightRequiring";
    String WATERING_TAG = "watering";
    String MULTIPLYING_TAG = "multiplying";
    String MEASURE_TAG = "measure";
    String AVE_LEN_FLOWER_TAG = "aveLenFlower";
    String LEAF_COLOUR_TAG = "leafColour";
    String STEM_COLOUR_TAG = "stemColour";
    String GROWING_TIPS_TAG = "growingTips";
    String VISUAL_PARAMETERS_TAG = "visualParameters";
    String ORIGIN_TAG = "origin";
    String SOIL_TAG = "soil";
    String NAME_TAG = "name";
    String FLOWER_TAG = "flower";
}
