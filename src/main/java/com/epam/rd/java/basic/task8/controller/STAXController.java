package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.TagConstants.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {


	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parse() {
		List<Flower> result = new ArrayList<>();
		Flower flower = null;
		VisualParameters vp = null;
		GrowingTips gt = null;

		try {
			XMLEventReader xmlReader = XMLInputFactory.newInstance().createXMLEventReader(xmlFileName,
					new FileInputStream(xmlFileName));

			while (xmlReader.hasNext()) {
				XMLEvent event = xmlReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					String name = startElement.getName().getLocalPart();

					switch (name) {
						case FLOWER_TAG:
							flower = new Flower();
							break;
						case NAME_TAG:
							event = xmlReader.nextEvent();
							assert flower != null;
							flower.setName(event.asCharacters().getData());
							break;
						case SOIL_TAG:
							event = xmlReader.nextEvent();
							assert flower != null;
							flower.setSoil(event.asCharacters().getData());
							break;
						case ORIGIN_TAG:
							event = xmlReader.nextEvent();
							assert flower != null;
							flower.setOrigin(event.asCharacters().getData());
							break;
						case VISUAL_PARAMETERS_TAG:
							vp = new VisualParameters();
							break;
						case GROWING_TIPS_TAG:
							gt = new GrowingTips();
							break;
						case STEM_COLOUR_TAG:
							event = xmlReader.nextEvent();
							assert vp != null;
							vp.setStemColor(event.asCharacters().getData());
							break;
						case LEAF_COLOUR_TAG:
							event = xmlReader.nextEvent();
							assert vp != null;
							vp.setLeafColour(event.asCharacters().getData());
							break;
						case AVE_LEN_FLOWER_TAG: {
							event = xmlReader.nextEvent();
							AveLenFlower aveLenFlower = new AveLenFlower();
							aveLenFlower.setContent(Integer.parseInt(event.asCharacters().getData()));
							Attribute attr = startElement.getAttributeByName(new QName(MEASURE_TAG));
							aveLenFlower.setMeasure(attr.getValue());
							assert vp != null;
							vp.setAveLenFlower(aveLenFlower);

							break;
						}
						case TEMPRETURE_TAG: {
							event = xmlReader.nextEvent();
							GrowingTips.Temperature temperature = new GrowingTips.Temperature();
							temperature.setContent(Integer.parseInt(event.asCharacters().getData()));
							Attribute attr = startElement.getAttributeByName(new QName(MEASURE_TAG));
							temperature.setMeasure(attr.getValue());
							assert gt != null;
							gt.setTemperature(temperature);
							break;
						}
						case LIGHTING_TAG: {
							Attribute attr = startElement.getAttributeByName(new QName(LIGHT_REQUIRING_TAG));
							assert gt != null;
							gt.setLightRequiring(LightRequiring.valueOf(attr.getValue()));

							break;
						}
						case WATERING_TAG: {
							event = xmlReader.nextEvent();
							Watering watering = new Watering();
							watering.setContent(Integer.parseInt(event.asCharacters().getData()));
							Attribute attr = startElement.getAttributeByName(new QName(MEASURE_TAG));
							watering.setMeasure(attr.getValue());
							assert gt != null;
							gt.setWatering(watering);

							break;
						}
						case MULTIPLYING_TAG:
							event = xmlReader.nextEvent();
							assert flower != null;
							flower.setMultiplying(event.asCharacters().getData());
							break;
					}
				} else if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					String name = endElement.getName().getLocalPart();
					switch (name) {
						case FLOWER_TAG:
							result.add(flower);
							flower = null;
							break;
						case VISUAL_PARAMETERS_TAG:
							assert flower != null;
							flower.setVisualParameters(vp);
							vp = null;
							break;
						case GROWING_TIPS_TAG:
							assert flower != null;
							flower.setGrowingTips(gt);
							gt = null;
							break;
					}
				}
			}

		} catch (FileNotFoundException | XMLStreamException | FactoryConfigurationError e) {
			e.printStackTrace();
		}

		return result;
	}
}