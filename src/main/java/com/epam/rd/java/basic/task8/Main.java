package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		if (args.length != 1) {
			return;
		}
		StAXMarshallingController stAXMarshallingController = new StAXMarshallingController();

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		// DOM
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowersDOM = domController.getDocumentFlowerItems();
		flowersDOM.sort(Comparator.comparing(Flower::getName));
		String outputXmlFile = "output.dom.xml";
		stAXMarshallingController.marshall(outputXmlFile,flowersDOM);

		// SAX
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> flowersSAX = saxController.parse();
		flowersSAX.sort(Comparator.comparing(Flower::getOrigin));
		outputXmlFile = "output.sax.xml";
		stAXMarshallingController.marshall(outputXmlFile,flowersSAX);

		// StAX
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowersSTAX = staxController.parse();
		flowersSTAX.sort(Comparator.comparing(Flower::getSoil));
		outputXmlFile = "output.stax.xml";
		stAXMarshallingController.marshall(outputXmlFile,flowersSTAX);

	}

}
