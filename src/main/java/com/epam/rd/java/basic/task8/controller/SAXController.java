package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private final String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parse() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SaxParserHandler handler = new SaxParserHandler();
		SAXParser parser = null;
		try {
			parser = factory.newSAXParser();
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
		File file = new File(xmlFileName);

		try {
			assert parser != null;
			parser.parse(file, handler);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		return handler.getResult();
	}

}