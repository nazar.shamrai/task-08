package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.TagConstants.*;

public class StAXMarshallingController {
    public void marshall(String xmlFileName, List<Flower> flowers){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            writeXml(out,flowers);
            String xml = out.toString(StandardCharsets.UTF_8);
            String prettyPrintXML = formatXML(xml);
            Files.writeString(Paths.get(xmlFileName), prettyPrintXML, StandardCharsets.UTF_8);
        } catch (XMLStreamException | TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

    private String formatXML(String xml) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));
        return output.toString();
    }

    private void writeXml(OutputStream out,List<Flower> flowers) throws XMLStreamException {

        XMLOutputFactory output = XMLOutputFactory.newInstance();

        XMLStreamWriter writer = output.createXMLStreamWriter(out);

        writer.writeStartDocument();

        writer.writeStartElement("flowers");
        writer.writeAttribute("xmlns","http://www.nure.ua");
        writer.writeAttribute(" xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");

        for (Flower flower:flowers) {
            writer.writeStartElement(FLOWER_TAG);
            writeFlowerNameSoilOrigin(writer, flower);
            writeVisualParameter(writer, flower);
            writeGrovingTips(writer, flower);
            writeMultiplying(writer, flower);
            writer.writeEndElement();
        }

        writer.writeEndElement();

        writer.writeEndDocument();

        writer.flush();

        writer.close();
    }

    private void writeMultiplying(XMLStreamWriter writer, Flower flower) throws XMLStreamException {
        writer.writeStartElement(MULTIPLYING_TAG);
        writer.writeCharacters(flower.getMultiplying());
        writer.writeEndElement();
    }

    private void writeGrovingTips(XMLStreamWriter writer, Flower flower) throws XMLStreamException {
        writer.writeStartElement(GROWING_TIPS_TAG);

        writer.writeStartElement(TEMPRETURE_TAG);
        writer.writeAttribute(MEASURE_TAG,String.valueOf(flower.getGrowingTips().getTemperature().getMeasure()));
        writer.writeCharacters(String.valueOf(flower.getGrowingTips().getTemperature().getContent()));
        writer.writeEndElement();

        writer.writeStartElement(LIGHTING_TAG);
        writer.writeAttribute(LIGHT_REQUIRING_TAG,flower.getGrowingTips().getLightRequiring().toString());
        writer.writeEndElement();

        writer.writeStartElement(WATERING_TAG);
        writer.writeAttribute(MEASURE_TAG,String.valueOf(flower.getGrowingTips().getWatering().getMeasure()));
        writer.writeCharacters(String.valueOf(flower.getGrowingTips().getWatering().getContent()));
        writer.writeEndElement();

        writer.writeEndElement();
    }

    private void writeFlowerNameSoilOrigin(XMLStreamWriter writer, Flower flower) throws XMLStreamException {
        writer.writeStartElement(NAME_TAG);
        writer.writeCharacters(flower.getName());
        writer.writeEndElement();

        writer.writeStartElement(SOIL_TAG);
        writer.writeCharacters(flower.getSoil());
        writer.writeEndElement();

        writer.writeStartElement(ORIGIN_TAG);
        writer.writeCharacters(flower.getOrigin());
        writer.writeEndElement();
    }

    private void writeVisualParameter(XMLStreamWriter writer, Flower flower) throws XMLStreamException {
        writer.writeStartElement(VISUAL_PARAMETERS_TAG);

        writer.writeStartElement(STEM_COLOUR_TAG);
        writer.writeCharacters(flower.getVisualParameters().getStemColor());
        writer.writeEndElement();

        writer.writeStartElement(LEAF_COLOUR_TAG);
        writer.writeCharacters(flower.getVisualParameters().getLeafColour());
        writer.writeEndElement();

        writer.writeStartElement(AVE_LEN_FLOWER_TAG);
        writer.writeAttribute(MEASURE_TAG,String.valueOf(flower.getVisualParameters().getAveLenFlower().getMeasure()));
        writer.writeCharacters(String.valueOf(flower.getVisualParameters().getAveLenFlower().getContent()));
        writer.writeEndElement();

        writer.writeEndElement();
    }

}
